package ito.poo.clases;

import java.time.LocalDate;

public class Lavado extends Procesos {
	
	private  int capacidad_litros;
	private int tiempo_lavadoXbotella;

public Lavado(String descripcion, LocalDate fecha_adquisicion, 
		float costoXmaquina ,int capacidad_litros, int tiempo_lavadoXbotella) {
	
		super(descripcion,fecha_adquisicion,costoXmaquina);
		this.capacidad_litros = capacidad_litros;
		this.tiempo_lavadoXbotella = tiempo_lavadoXbotella;
	}

public float obtenerCostoLavadoXbotella() {
	 return 0f;
}



public int getTiempo_lavadoXbotella() {
	return tiempo_lavadoXbotella;
}

public void setTiempo_lavadoXbotella(int tiempo_lavadoXbotella) {
	this.tiempo_lavadoXbotella = tiempo_lavadoXbotella;
}

public int getCapacidad_litros() {
	return capacidad_litros;
}

@Override
public String toString() {
	return "Lavado [capacidad_litros=" + capacidad_litros + ", tiempo_lavadoXbotella=" + tiempo_lavadoXbotella
			+ ", toString()=" + super.toString() + "]";
}



}
