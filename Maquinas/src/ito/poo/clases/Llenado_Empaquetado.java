package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Llenado_Empaquetado extends Procesos {
	private int cantidadembasesllenosXminuto;
	private ArrayList<String>lista_regularizacion;
	
	
public Llenado_Empaquetado(String descripcion, LocalDate fecha_adquisicion, float costoXmaquina,
			int cantidadembasesllenosXminuto) {
		super(descripcion, fecha_adquisicion, costoXmaquina);
		this.cantidadembasesllenosXminuto = cantidadembasesllenosXminuto;
		this.lista_regularizacion=new ArrayList<String>();
	}
public float obtenerCostoLlenadoEmbasadoXbotella() {
	 return 0f;
}



public int getCantidadembasesllenosXminuto() {
	return cantidadembasesllenosXminuto;
}


public void setCantidadembasesllenosXminuto(int cantidadembasesllenosXminuto) {
	this.cantidadembasesllenosXminuto = cantidadembasesllenosXminuto;
}


public ArrayList<String> getLista_regularizacion() {
	return lista_regularizacion;
}


@Override
public String toString() {
	return "Llenado_Empaquetado [cantidadembasesllenosXminuto=" + cantidadembasesllenosXminuto
			+ ", lista_regularizacion=" + lista_regularizacion + ", toString()=" + super.toString() + "]";
}



}
