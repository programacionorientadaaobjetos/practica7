package ito.poo.clases;

import java.time.LocalDate;

public class Empaquetado extends Procesos {
	private String tipo_empaque;
	private int cantidadempaquesXminuto;
	
public Empaquetado(String descripcion, LocalDate fecha_adquisicion, float costoXmaquina, String tipo_empaque,
			int cantidadempaquesXminuto) {
		super(descripcion, fecha_adquisicion, costoXmaquina);
		this.tipo_empaque = tipo_empaque;
		this.cantidadempaquesXminuto = cantidadempaquesXminuto;
	}
public float obtenerCostoEmpaquetadoXbotella() {
	 return 0f;
}




public String getTipo_empaque() {
	return tipo_empaque;
}

public void setTipo_empaque(String tipo_empaque) {
	this.tipo_empaque = tipo_empaque;
}

public int getCantidadempaquesXminuto() {
	return cantidadempaquesXminuto;
}

public void setCantidadempaquesXminuto(int cantidadempaquesXminuto) {
	this.cantidadempaquesXminuto = cantidadempaquesXminuto;
}

@Override
public String toString() {
	return "Empaquetado [tipo_empaque=" + tipo_empaque + ", cantidadempaquesXminuto=" + cantidadempaquesXminuto
			+ ", toString()=" + super.toString() + "]";
}

}
